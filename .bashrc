tmux
clear

export EDITOR=vim

if ! pidof obs > /dev/null 2> /dev/null
then
	IP=`curl -4 v4.ifconfig.co 2> /dev/null`
	export PS1="$USER@$IP\n$ "
else
	export PS1="$USER@censored\n$ "
fi

# Make C^s not freeze the terminal
[[ $- == *i* ]] && stty -ixon

cd ~/main

#set -o vi

alias q=exit

alias p=cat
alias lowercase="tr [:upper:] [:lower:]"
alias unique="sort -u"
alias makewords="\\grep -o -E '\w+'"
alias s="xset dpms force off"

alias l="ls -1 --color"
alias sl="ls -A1 --color"
alias cls="clear; ls -1 --color"
alias csl="clear; ls -A1 --color"
alias cl="clear"
alias cll="cd .."
alias r="ranger"
alias v=vim

alias vv="vim main.*"
alias b="vim ~/.bashrc"
alias vr="vim ~/.vimrc"

#alias medibang="wine64 ~/.wine/dosdevices/c:/\"Program Files\"/Medibang/\"MediBang Paint Pro\"/MediBangPaintPro.exe"
alias fix='echo -e "\033c" ; stty sane; setterm -reset; reset; tput reset; clear'
alias displayfix="export DISPLAY=:0"
